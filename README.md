# Counterfactual Fairness

## Final project for the discipline of Probabilistic Graphical Models (PGM)

UFMG - DCC

This repository has Python and Stan code for the paper:

Matt J. Kusner\*, Joshua R. Loftus\*, Chris Russell\*, Ricardo Silva\
[Counterfactual Fairness](https://arxiv.org/pdf/1703.06856.pdf)\
Neural Information Processing Systems (NIPS), 2017\
\*=authors contributing equally


Files:

Python main file        = main.py

Jupyter notebook python = main.ipynb

Data about this paper   = law_data.csv

Model trained           = model_z.pkl (train) and model_z_t.pkl (test)
