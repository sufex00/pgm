#@title Imports (RUN ME!) { display-mode: "form" }

import pystan
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

import pickle

from sklearn.model_selection import train_test_split

sns.set()  # Nice plot aesthetic
np.random.seed(101)

model = """

data {
  int<lower = 0> N; // number of observations
  int<lower = 0> K; // number of covariates
  matrix[N, K]   a; // sensitive variables
  real           ugpa[N]; // UGPA
  int            lsat[N]; // LSAT
  real           zfya[N]; // ZFYA

}

transformed data {

 vector[K] zero_K;
 vector[K] one_K;

 zero_K = rep_vector(0,K);
 one_K = rep_vector(1,K);

}

parameters {

  vector[N] u;

  real ugpa0;
  real eta_u_ugpa;
  real lsat0;
  real eta_u_lsat;
  real eta_u_zfya;

  vector[K] eta_a_ugpa;
  vector[K] eta_a_lsat;
  vector[K] eta_a_zfya;


  real<lower=0> sigma_g_Sq;
}

transformed parameters  {
 // Population standard deviation (a positive real number)
 real<lower=0> sigma_g;
 // Standard deviation (derived from variance)
 sigma_g = sqrt(sigma_g_Sq);
}

model {

  // don't have data about this
  u ~ normal(0, 1);

  ugpa0      ~ normal(0, 1);
  eta_u_ugpa ~ normal(0, 1);
  lsat0     ~ normal(0, 1);
  eta_u_lsat ~ normal(0, 1);
  eta_u_zfya ~ normal(0, 1);

  eta_a_ugpa ~ normal(zero_K, one_K);
  eta_a_lsat ~ normal(zero_K, one_K);
  eta_a_zfya ~ normal(zero_K, one_K);

  sigma_g_Sq ~ inv_gamma(1, 1);

  // have data about these
  ugpa ~ normal(ugpa0 + eta_u_ugpa * u + a * eta_a_ugpa, sigma_g);
  lsat ~ poisson(exp(lsat0 + eta_u_lsat * u + a * eta_a_lsat));
  zfya ~ normal(eta_u_zfya * u + a * eta_a_zfya, 1);

}

"""

fields = ['race', 'sex', 'LSAT', 'UGPA', 'region_first', 'ZFYA',
          'sander_index', 'first_pf']

data = pd.read_csv("law_data.csv", usecols = fields)

data.head()

def as_numeric(df, col, value, new_col):
    data1 = pd.DataFrame(df)
    data1[new_col] = df[col] == value
    data1[new_col][df[new_col] == False] = int(0)
    data1[new_col][df[new_col] == True] = int(1)
    return data1


data2 = as_numeric(data, 'race', 'Amerindian', 'amerind')
data2 = as_numeric(data2, 'race', 'Asian', 'asian')
data2 = as_numeric(data2, 'race', 'Black', 'black')
data2 = as_numeric(data2, 'race', 'Hispanic', 'hisp')
data2 = as_numeric(data2, 'race', 'Mexican', 'mexican')
data2 = as_numeric(data2, 'race', 'Other', 'other')
data2 = as_numeric(data2, 'race', 'Puertorican', 'puerto')
data2 = as_numeric(data2, 'race', 'White', 'white')

data2 = as_numeric(data2, 'sex', 1, 'female')
data2 = as_numeric(data2, 'sex', 2, 'male')

data2.head()


sense_cols = ["amerind", "asian", "black", "hisp", "mexican", "other", "puerto", "white", "male", "female"]

columns = {}

for idx, e in enumerate(data.columns):
    columns[e] = idx


data_matrix = data.values

columns

X_train, X_test= train_test_split(data_matrix,test_size=0.2)

X_test_dic = {'N' : len(X_test), 'K' : len(sense_cols), 'a': X_test[:,[columns[e] for e in sense_cols]].tolist(), 
          'ugpa' : X_test[:, columns['UGPA']].tolist(), 
          'lsat' : [int(e) for e in X_test[:, columns['LSAT']].tolist()], 
          'zfya' : X_test[:, columns['ZFYA']].tolist()}


sm_t = pystan.StanModel(model_code=model)

fit_t = sm_t.sampling(data=X_test_dic, iter=2000, chains=4, warmup=1000, thin=1, seed=101,
                 verbose=True)

with open('model_z.pkl', 'wb') as f:
    pickle.dump(fit_t.extract(), f)


